#include "Gun.h"
#include "TextureLoad.h"

Gun::Gun()
	:m_position(0, 0),
	m_damage(10),
	m_level(1),
	m_form(sf::Quads,4)
{
	m_texture.loadFromFile(GUN);

	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX()+5, m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX()+5, m_position.getPositionY()+10);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY()+10);

	m_form[0].texCoords = sf::Vector2f(0.f, 0.f);
	m_form[1].texCoords = sf::Vector2f(8.5f, 0.f);
	m_form[2].texCoords = sf::Vector2f(8.5f, 30.f);
	m_form[3].texCoords = sf::Vector2f(0.f, 30.f);


}

Gun::Gun(uint8_t damage, uint8_t level,float speed,Position position)
	:m_form(sf::Quads, 4)
{
	m_damage = damage;
	m_level = level;
	m_speed = speed;
	m_position = position;

	m_texture.loadFromFile(GUN);

	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX() + 5, m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX() + 5, m_position.getPositionY() + 10);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY() + 10);

	m_form[0].texCoords = sf::Vector2f(0.f, 0.f);
	m_form[1].texCoords = sf::Vector2f(8.5f, 0.f);
	m_form[2].texCoords = sf::Vector2f(8.5f, 30.f);
	m_form[3].texCoords = sf::Vector2f(0.f, 30.f);
	
}

Gun::Gun(const Gun& gun)
	:m_form(sf::Quads, 4)

{
	this->m_position = gun.m_position;
	this->m_speed = gun.m_speed;
	this->m_damage = gun.m_damage;
	this->m_level = gun.m_level;
	
	m_texture.loadFromFile(GUN);
	
	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX() + 5, m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX() + 5, m_position.getPositionY() + 10);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY() + 10);

	m_form[0].texCoords = sf::Vector2f(0.f, 0.f);
	m_form[1].texCoords = sf::Vector2f(8.5f, 0.f);
	m_form[2].texCoords = sf::Vector2f(8.5f, 30.f);
	m_form[3].texCoords = sf::Vector2f(0.f, 30.f);
}

uint8_t Gun::getDamage()
{
	return m_damage;
}

uint8_t Gun::getlevel()
{
	return m_level;

}

void Gun::setDamage(uint8_t dmg)
{
	m_damage = dmg;
}

void Gun::setLevel(uint8_t level)
{
	m_level = level;

}

void Gun::setSpeed(float speed)
{
	m_speed = speed;
}

void Gun::move(float x, float y)
{
	m_position.setPositionX(m_position.getPositionX() + x);
	m_position.setPositionY(m_position.getPositionY() + y);
	
	m_form[0].position += sf::Vector2f(x, y);
	m_form[1].position += sf::Vector2f(x, y);
	m_form[2].position += sf::Vector2f(x, y);
	m_form[3].position += sf::Vector2f(x, y);

}

void Gun::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	// draw the vertex array
	target.draw(m_form, states);
}



float Gun::getSpeed()
{
	return m_speed;
}
Position Gun::getPosition()
{
	return m_position;
}
sf::Texture& Gun::getTexture()
{
	return m_texture;
}
sf::VertexArray Gun::getForm()
{
	return m_form;
}
void Gun::setPosition(Position position)
{
	this->m_position = position;

	m_form[0].position = sf::Vector2f(position.getPositionX(), position.getPositionY());
	m_form[1].position = sf::Vector2f(position.getPositionX() + 5, position.getPositionY());
	m_form[2].position = sf::Vector2f(position.getPositionX() + 5, position.getPositionY() + 10);
	m_form[3].position = sf::Vector2f(position.getPositionX(), position.getPositionY() + 10);
}