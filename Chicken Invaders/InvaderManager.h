#pragma once
#include <vector>

#include "chicken.h"
#include "Texture.h"
#include "Asteroid.h"
#include "Collision.h"
class InvaderManager
{
public:
	InvaderManager();
	static void insertChicken(chicken&);
	static void drawChickens(sf::RenderWindow&,Texture&);
	static std::vector<chicken> m_chickens;
	static void MoveChickens(Position pos);

	static void insertAsteroid(Asteroid&);
	static void drawAsteroid(sf::RenderWindow&, Texture&);
	static std::vector<Asteroid> m_asteroids;
	static void MoveAsteroids(Position pos,const sf::RenderWindow& window);
	
private:
	unsigned m_aliveInvaders;
	bool m_hasAllInvadersBeenAdded = false;
	bool m_isMovingLeft = false;
	bool m_moveDown = false;
};
