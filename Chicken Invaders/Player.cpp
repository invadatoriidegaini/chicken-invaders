#include "Player.h"
#include "TextureLoad.h"
#include "Texture.h"

Player::Player()
	:m_position(0.f, 0.f),
	m_lifes(0),
	m_rockets(),
	m_gun(),
	m_quantity(),
	m_score(0),
	m_speed(1),
	m_size(60),
	m_form(sf::Quads,4)
{
	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY() + m_size);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY() + m_size);


	m_form[0].texCoords = sf::Vector2f(0.f, 0.f);
	m_form[1].texCoords = sf::Vector2f(200.f, 0.f);
	m_form[2].texCoords = sf::Vector2f(200.f, 200.f);
	m_form[3].texCoords = sf::Vector2f(0.f, 200.f);

}


Player::Player(Position position, uint8_t lifes, Rockets rockets, Gun gun, Food quantity, unsigned long int score, int speed)
	:m_position(position),
	m_lifes(lifes),
	m_rockets(rockets),
	m_gun(gun),
	m_quantity(quantity),
	m_score(score),
	m_speed(speed),
	m_size(60),
	m_form(sf::Quads,4)
{
	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY() + m_size);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY() + m_size);

	m_form[0].texCoords = sf::Vector2f(0.f, 0.f);
	m_form[1].texCoords = sf::Vector2f(200.f, 0.f);
	m_form[2].texCoords = sf::Vector2f(200.f, 200.f);
	m_form[3].texCoords = sf::Vector2f(0.f, 200.f);
}

Player::Player(const Player & player)
	:m_position(player.m_position),
	m_lifes(player.m_lifes),
	m_rockets(player.m_rockets),
	m_gun(player.m_gun),
	m_quantity(player.m_quantity),
	m_score(player.m_score),
	m_speed(player.m_speed),
	m_size(60),
	m_form(sf::Quads,4)
{
	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY() + m_size);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY() + m_size);

	m_form[0].texCoords = sf::Vector2f(0.f, 0.f);
	m_form[1].texCoords = sf::Vector2f(200.f, 0.f);
	m_form[2].texCoords = sf::Vector2f(200.f, 200.f);
	m_form[3].texCoords = sf::Vector2f(0.f, 200.f);
}

Position Player::getPosition() const
{
	return m_position;
}

int8_t Player::getLife() const
{
	return m_lifes;
}

Rockets& Player::getRockets()
{
	return m_rockets;
}

Gun& Player::getGun()
{
	return m_gun;
}

Food Player::getQuantity() const
{
	return m_quantity;
}

unsigned long int Player::getScore() const
{
	return m_score;
}

int Player::getSpeed()
{
	return m_speed;
}

int Player::getSize()
{
	return m_size;
}

sf::VertexArray Player::getPlayerDrawableContent()
{
	return m_form;
}

void Player::setPosition(Position position)
{
	this->m_position = position;

	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY() + m_size);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY() + m_size);
	
}

void Player::setLife(int8_t life)
{
	this->m_lifes = life;
}

void Player::setRockets(Rockets rockets)
{
	this->m_rockets = rockets;
}

void Player::setGun(Gun gun)
{
	this->m_gun = gun;
}

void Player::setQuantity(Food quantity)
{
	this->m_quantity = quantity;
}

void Player::setScore(unsigned long int score)
{
	this->m_score = score;
}

void Player::setSpeed(int speed)
{
	this->m_speed = speed;
}

void Player::setSize(int size)
{
	m_size = size;
}

void Player::move(float x, float y)
{
	m_position.setPositionX(m_position.getPositionX() + x);
	m_position.setPositionY(m_position.getPositionY() + y);
	
	m_form[0].position += sf::Vector2f(x, y);
	m_form[1].position += sf::Vector2f(x, y);
	m_form[2].position += sf::Vector2f(x, y);
	m_form[3].position += sf::Vector2f(x, y);
}

void Player::setFireGun(bool shooting_gun)
{
	if (shooting_gun)
		is_fire_gun = true;
	else
		is_fire_gun = false;

}

void Player::setFireRocket(bool shooting_rocket)
{
	if (shooting_rocket)
		is_fire_rocket = true;
	else
		is_fire_rocket = false;
}


Player& Player::operator=(const Player& player)
{
	this->m_lifes = player.m_lifes;
	this->m_position = player.m_position;
	this->m_rockets = player.m_rockets;
	this->m_gun = player.m_gun;
	this->m_quantity = player.m_quantity;
	this->m_score = player.m_score;
	this->m_form = player.m_form;
	return *this;
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_form, states);
}

