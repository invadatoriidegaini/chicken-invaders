#pragma once
#include "Position.h"
#include "Rockets.h"
#include <iostream>
#include "Gun.h"
#include "Food.h"
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

class Player: public sf::Drawable
{
public:
	
	//CONSTRUCTORS & DESTRUCTORS
	
	Player();
	Player(Position position, uint8_t lifes, Rockets rockets, Gun gun, Food quantity, unsigned long int score, int speed);
	Player(const Player &player);
	Player& operator= (const Player& player);

	~Player() = default;
	
	//GETTERS
	
	Position getPosition() const;
	int8_t getLife() const;
	Rockets& getRockets();
	Gun& getGun();
	Food getQuantity() const;
	unsigned long int getScore() const;
	int getSpeed();
	int getSize();
	sf::VertexArray getPlayerDrawableContent();
	
	//SETTERS
	
	void setPosition(Position position);
	void setLife(int8_t life);
	void setRockets(Rockets rockets);
	void setGun(Gun gun);
	void setQuantity(Food quantity);
	void setScore(unsigned long int score);
	void setSpeed(int speed);
	void setSize(int size);
	
	//FUNCTIONS
	
	void move(float x, float y);

	void setFireGun(bool shooting_gun);
	void setFireRocket(bool shooting_rocket);

private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public: 

	bool is_fire_rocket = false;
	bool is_fire_gun = false;

	sf::VertexArray m_form;

private:

	Position m_position;
	int8_t m_lifes;
	Rockets m_rockets;
	Gun m_gun;
	Food m_quantity;
	int m_speed;
	int m_size;
	unsigned long int m_score;

};
