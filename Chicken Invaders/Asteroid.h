#pragma once
#include <iostream>
#include "Position.h"
//#include "Game.h"
#include <SFML/Graphics.hpp>
constexpr auto DEG2RAD = 3.14159f/180.0f;
class Asteroid: public sf::Drawable
{
public:
	enum class Size :uint8_t
	{
	GIANT=90,
	MEDIUM=60,
	SMALL=30,
	Unknown=5
	};
	Asteroid();
	Asteroid(uint8_t health,uint8_t damage,Size size,Position position,float speed,float angle,bool is_alive);
	uint8_t GetHealth() const;
	uint8_t GetDamage() const;
	Size GetSize() const;
	Position GetPosition() const;
	float GetSpeed() const;
	float GetAngle() const;
	void SetHealth(uint8_t health);
	void SetDamage(uint8_t damage);
	void SetSize(Size size);
	void SetPosition(Position position);
	void SetSpeed(uint8_t speed);
	void SetAngle(float angle);
	void SetDirection(float x,float y);
	Asteroid(const Asteroid& asteroid);
	friend std::ostream& operator <<(std::ostream& out, const Asteroid& asteroid);
	void move();
	bool isAlive();
	
	bool CheckPoint(sf::Vector2f point);
	void Update();
private:
	virtual void draw(sf::RenderTarget& target,sf::RenderStates states)const ;
private:
	uint8_t m_health;
	uint8_t m_damage;
	Size m_size;
	Position m_position;
	float m_speed;
	float m_angle;
	bool m_is_alive;
	sf::Vector2f direction;
	sf::CircleShape shape;
	static const float radius[4];
	//Game m_width;
	//Game m_height;
	sf::VertexArray m_form;

};

