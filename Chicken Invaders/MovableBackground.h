#include <SFML\Graphics.hpp>
#include <iostream>

class MovableBackground
{
public:
	MovableBackground();
	MovableBackground(sf::RenderWindow& window);
	void Update(sf::RenderWindow& window, float elapsedTime);
	void Render(sf::RenderWindow& window);

private:
	sf::Texture bgTex;

	sf::RectangleShape bgShape1, bgShape2;
	sf::Vector2f bgSize;

	float bgSpeed;
	float bgY1;
    float bgY2;
};