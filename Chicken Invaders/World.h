#pragma once
#include <iostream>
#include <vector>

#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "SFML/Network.hpp"
#include "SFML/System.hpp"
#include "SFML/Window.hpp"
#include "SFML/OpenGL.hpp"

#include "MovableBackground.h"
#include "Player.h"
#include "Wave.h"
#include "Texture.h"

class World
{

	//Private Function
private:
	void initializeVariables();
	void initWindow();

	//CONSTRUCTORS AND DESTRUCTORS
public:
	World();
	virtual ~World();

	//METHODS
	void updateEvents();
	void update();
	void render();
	void gameStart(World& world);
	//SETTERS AND GETTERS

	sf::RenderWindow* getWindow();

	void setHeightResolution(const int&);
	void setWidthResolution(const int&);
	int getHeightResolution() const;
	int getWidhtResolution() const;
	//ACCESORS FUNCTION
	const bool running() const;
	//FIELDS
private:

	Wave m_wave;

	Texture m_texture;
	MovableBackground* m_background;
	Player m_player;

	std::vector <Gun> bullets;
	
	//Window
	sf::RenderWindow* m_window;
	sf::VideoMode m_videomode;
	sf::Event m_ev;

	sf::Font m_font;
	sf::Text m_text;
	sf::Clock m_clock;

	int m_width, m_height;
	int comutator = 1;

	bool m_isMovingLeft = false;
};



