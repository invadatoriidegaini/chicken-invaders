#pragma once
#include <iostream>
#include "Position.h"

class Food 
{
public:
	Food();
	Food(uint16_t amount, double viteza, Position position);
	Food& operator ++();
	bool generare();
	~Food();

	void setQuantity(uint16_t amount);
	void setPosition(Position position);
	uint16_t getQuantity() const;
	Position getPosition() const;
	float getSpeed() const;
	void move(float x, float y);

private:
	uint16_t m_amount;
	float m_viteza;
	Position m_position;
};