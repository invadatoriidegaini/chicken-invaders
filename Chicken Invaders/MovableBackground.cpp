#include "MovableBackground.h"
#include "TextureLoad.h"

MovableBackground::MovableBackground()
{
}

MovableBackground::MovableBackground(sf::RenderWindow& window)
{
	bgSpeed = 0.3f;

	bgTex.loadFromFile(BACKGROUND);
	bgTex.setSmooth(false);
	bgTex.setRepeated(true);

	bgY1 = bgShape1.getPosition().y;
	bgY2 = bgShape2.getPosition().y - (float) window.getSize().y;


	bgSize.x = (float) window.getSize().x;
	bgSize.y = (float) window.getSize().y;

	bgShape1.setTexture(&bgTex);
	bgShape1.setSize(bgSize);

	bgTex.loadFromFile(BACKGROUNDF);
	bgTex.setSmooth(false);
	bgTex.setRepeated(true);

	bgShape2.setTexture(&bgTex);
	bgShape2.setSize(bgSize);

}


void MovableBackground::Update(sf::RenderWindow& window, float elapsedTime)
{
	
	if (bgY1 < (float) window.getSize().y)
	{
		bgY1 += bgSpeed * elapsedTime;
	}
	else
	{
		bgY1 = 0;
	}
	bgShape1.setPosition(0.f, bgY1);
	if (bgY2 < 0)
	{
		bgY2 += bgSpeed * elapsedTime;
	}
	else
	{
		bgY2 = -(float) window.getSize().y;
	}
	bgShape2.setPosition(0.f, bgY2);

}
void MovableBackground::Render(sf::RenderWindow& window)
{
	window.draw(bgShape1);
	window.draw(bgShape2);
}