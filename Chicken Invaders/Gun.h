#pragma once
#include "Weapon.h"
#include "Position.h"
#include <SFML/Graphics.hpp>

class Gun : public Weapon,public sf::Drawable
{
public:
	//CONSTRUCTORS
	Gun();
	Gun(uint8_t, uint8_t,float,Position);
	Gun(const Gun&);
	//~Gun();
	//GETTERS
	uint8_t getDamage();
	uint8_t getlevel();
	float getSpeed();
	Position getPosition();
	sf::Texture& getTexture();
	sf::VertexArray getForm();
	//SETTERS
	void setDamage(uint8_t);
	void setLevel(uint8_t);
	void setSpeed(float);
	void setPosition(Position );
	//METHODS
	void move(float x, float y);

private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public:
	sf::Texture m_texture;
	sf::VertexArray m_form;

private:
	float m_speed;
	uint8_t m_damage;
	uint8_t m_level;
	Position m_position;
	
};
