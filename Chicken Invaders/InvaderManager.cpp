#include "InvaderManager.h"

std::vector<chicken> InvaderManager::m_chickens= {};
std::vector<Asteroid> InvaderManager::m_asteroids = {};

InvaderManager::InvaderManager()
{
}


void InvaderManager::insertChicken(chicken& chicken)
{

	m_chickens.push_back(chicken);
	//std::cout << " " << m_chickens.size() << std::endl;
}

void InvaderManager::drawChickens(sf::RenderWindow& window,Texture& texture)
{
	//int i = 0;
	for (chicken chick : m_chickens)
	{
		window.draw(chick, &texture.getChickenREDTexture());
		
		//window.draw(chick.getChickenDrawablecontent());
		//std::cout << i << " chicken drawed\n";
		//i++;
	}
}

void InvaderManager::MoveChickens(Position pos)
{
	for (chicken& chick : InvaderManager::m_chickens)
	{
		chick.move(pos.getPositionX(), pos.getPositionY());
	}
}

void InvaderManager::insertAsteroid(Asteroid& astero)
{
	m_asteroids.push_back(astero);
}

void InvaderManager::drawAsteroid(sf::RenderWindow& window, Texture& texture)
{
	
	for (Asteroid astero : m_asteroids)
	{
		//astero.Update(2);
		window.draw(astero, &texture.getAsteroidTexture());


	}
}

void InvaderManager::MoveAsteroids(Position pos,const sf::RenderWindow& window)
{	
	
	for (std::vector<Asteroid>::iterator it =m_asteroids.begin(); it != m_asteroids.end(); ++it)
	{
		(*it).SetDirection(pos.getPositionX(), pos.getPositionY());
		(*it).move();
		//if (Collision::asteroidCollidingFrame(*it)|| Collision::asteroidCollindingFrameRight(*it, window))
		//{
			//InvaderManager::m_asteroids.erase(it);
		//}
		
	}
}

