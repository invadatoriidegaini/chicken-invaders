#pragma once
#include "Player.h"
#include "chicken.h"
#include "Asteroid.h"
#include "Egg.h"
#include "Gift.h"
#include <vector>
#include "Boss.h"

class Collision : public Player, chicken, Asteroid, Egg, Gift
{
public:
	static bool playerEggColliding(Player&, Egg&);
	static bool playerChickenColliding(Player&, chicken&);
	static bool playerAsteroidColliding(Player&, Asteroid&);
	static bool gunCollidingChicken(Gun&, chicken&);/*de revazut*/
	static bool gunCollidingAsteroid(Gun&, Asteroid&);
	static bool asteroidCollindingFrameRight(const Asteroid& asteroids, const sf::RenderWindow& window);
	static bool asteroidCollindingFrameLeft(const Asteroid& asteroids);
	static bool giftCollidingPlayer(Gift&, Player&);
	static bool playerCollidingFood(Food&, Player&);
	//static bool gunCollidingBoss(Gun&, Boss&);
	static bool gunCollidingFrame(Gun&);
	static bool asteroidCollidingFrameDown(const Asteroid& asteroid, const sf::RenderWindow& window);
	static bool chickenCollindingFrameRight(const std::vector<chicken>& ,const sf::RenderWindow &window);
	static bool chickenCollindingFrameLeft(const std::vector<chicken>&);


};

