#include <iostream>
#include "chicken.h"

// CONSTRUCTORS AND DESTRUCTORS

chicken::chicken()
	:m_health(1),
	m_position(0,0),
	m_is_alive(true),
	m_size(67.5f),
	m_form(sf::Quads, 4)
{

	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY() + m_size);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY() + m_size);

	m_form[0].texCoords = sf::Vector2f(0.f, 0.f);
	m_form[1].texCoords = sf::Vector2f(200.f, 0.f);
	m_form[2].texCoords = sf::Vector2f(200.f, 200.f);
	m_form[3].texCoords = sf::Vector2f(0.f, 200.f);

};

chicken::chicken(Position position, int health, bool is_alive)
	:m_position(position),
	m_health(health),
	m_is_alive(is_alive),
	m_size(67.5f),
	m_form(sf::Quads, 4)
{
	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX() + m_size, m_position.getPositionY() + m_size);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY() + m_size);

	m_form[0].texCoords = sf::Vector2f(0.f, 0.f);
	m_form[1].texCoords = sf::Vector2f(200.f, 0.f);
	m_form[2].texCoords = sf::Vector2f(200.f, 200.f);
	m_form[3].texCoords = sf::Vector2f(0.f, 200.f);
}

chicken::chicken(const chicken& chicken)
	:m_health(chicken.m_health),
	m_is_alive(chicken.m_is_alive),
	m_position(chicken.m_position),
	m_chicken_texture(chicken.m_chicken_texture),
	m_size(67.5f),
	m_form(chicken.m_form)
{
}

chicken::~chicken()
{}

//GETTERS

int chicken::getSpeed() const
{
	return m_speed;
}

Position chicken::getPosition() const
{
	return m_position;
}

uint8_t chicken::getHealth() const
{
	return m_health;
}

sf::VertexArray chicken::getChickenDrawablecontent() 
{
	return m_form;
}

int chicken::getSize() const
{
	return m_size;
}

//SETTERS

void chicken::setSpeed(int speed)
{
	this->m_speed = speed;

}

void chicken::setHealth(uint8_t health)
{
	m_health -= health;
}

void chicken::setPosition(Position position)
{
	m_position = position;

	m_form[0].position = sf::Vector2f(position.getPositionX(), position.getPositionY());
	m_form[1].position = sf::Vector2f(position.getPositionX() + m_size, position.getPositionY());
	m_form[2].position = sf::Vector2f(position.getPositionX() + m_size, position.getPositionY() + m_size);
	m_form[3].position = sf::Vector2f(position.getPositionX(), position.getPositionY() + m_size);
}

void chicken::setSize(int size)
{
	m_size = size;
}

bool chicken::isAlive()
{
	if (m_health > static_cast <uint8_t> (0))
		return m_is_alive;
	else
		return !m_is_alive;
}

/*void chicken::setTexture()
{
	m_chicken_texture.loadFromFile(GUN);
}*/

//OTHER FUNCTIONS

void chicken::Attack()
{
	while (isAlive())
	{
		if (5 == 1 + rand() % 5)

			Egg(getPosition(), 2);

	}
}
void chicken::move(float x, float y)
{
	m_position.setPositionX(m_position.getPositionX() + x);
	m_position.setPositionY(m_position.getPositionY() + y);

	m_form[0].position += sf::Vector2f(x, y);
	m_form[1].position += sf::Vector2f(x, y);
	m_form[2].position += sf::Vector2f(x, y);
	m_form[3].position += sf::Vector2f(x, y);
}

void chicken::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	// draw the vertex array
	target.draw(m_form, states);
}


