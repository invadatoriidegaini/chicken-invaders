#pragma once
#include "Position.h"
#include "Weapon.h"
#include <SFML/Graphics.hpp>

class Egg
{
public:
	//CONSTRUCTORS
	Egg();
	Egg(Position, double);
	//SETTERS
	void setSpeed(double);
	//GETTERS
	double getSpeed() const;
	Position getPosition() const;
	//DESTRUCTORS
	~Egg();
	void move(float x, float y);
	void drawEgg(sf::RenderTarget&);

public:
	sf::Texture m_texture;
	sf::RectangleShape m_shape;
private:
	double m_speed;
	Position m_position;
};

