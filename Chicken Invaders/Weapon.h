#pragma once
#include <iostream>
class Weapon
{
public:

	//GETTERS
	virtual uint8_t getDamage() = 0;
	//SETTERS
	virtual void setDamage(uint8_t) = 0;
	
	//OPERATORS
	Weapon& operator ++();


};