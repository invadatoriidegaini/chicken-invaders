#include "Food.h"

Food::Food()
	:m_amount(0),
	m_position()
{
}

Food::Food(uint16_t amount, double viteza, Position position):m_amount(amount),m_viteza(viteza),m_position(position)
{
}

Food& Food::operator++()
{
	this->m_amount++;
	return *this;
}

Food::~Food()
{
}

void Food::setQuantity(uint16_t amount) 
{
	this->m_amount = amount;
}

void Food::setPosition(Position position)
{
	this->m_position = position;
}
uint16_t Food::getQuantity() const
{
	return m_amount;
}
Position Food::getPosition() const
{
	return m_position;
}

float Food::getSpeed() const
{
	return m_viteza;
}

void Food::move(float x, float y)
{
	m_position.setPositionX(m_position.getPositionX() + x);
	m_position.setPositionY(m_position.getPositionY() + y);
}

