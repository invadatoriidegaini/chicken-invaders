#include "Position.h"


Position::Position()
	:m_x(0),
	m_y(0)
{
}

Position::Position(float x, float y)
	: m_x(x),
	m_y(y)
{
}
Position::Position(const Position &position)
	: m_x(position.m_x),
	m_y(position.m_y)
{
}

const float Position::getPositionX() const
{
	return this->m_x;
}

const float Position::getPositionY() const
{
	return this->m_y;
}

void Position::setPositionX(float x)
{
	this->m_x = x;
}

void Position::setPositionY(float y)
{
	this->m_y = y;
}

Position & Position::operator=(const Position & position)
{
	if (this != &position)
	{
		this->m_x = position.m_x;
		this->m_y = position.m_y;
	}
	return *this;
}

bool operator==(const Position& pos1, const Position& pos2)
{
	if (pos1.getPositionX() == pos2.getPositionX() && pos1.getPositionY() == pos2.getPositionY())
	{
		return true;
	}
	return false;
}
