#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include <memory>
#include "Widget.h"
class StackMenu
{
public:
	StackMenu(const sf::RenderWindow& window, float baseY);
	StackMenu(const sf::Vector2f& position);
	StackMenu(StackMenu&& other);
	StackMenu& operator=(StackMenu&& other);
	~StackMenu() = default;
	void addWidget(std::unique_ptr<Widget>w);
	void setTitle(const std::string& tile, const sf::RenderTarget& target);
	void handleEvent(sf::Event e, const sf::RenderWindow& window);
	void render(sf::RenderTarget& renderer);
private:
	void initWidget(Widget& w);
	std::vector<std::unique_ptr<Widget>>m_widgets;
	sf::RectangleShape m_background;
	sf::Vector2f m_basePosition;
	sf::Vector2f m_baseSize;
	Widget::Text m_titleText;
};

