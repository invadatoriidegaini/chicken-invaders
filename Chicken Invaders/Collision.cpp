#include "Collision.h" // se incepe cu implementarea dupa ce stabilim cati pixeli vor avea fiecare 

bool Collision::playerEggColliding(Player& player, Egg& egg)
{
	if (player.getPosition() == egg.getPosition())  // trebuie calculat perimetrul navetei si al oului
	{
		return 1;
	}
	return 0;
}

bool Collision::playerChickenColliding(Player& player, chicken& chicken)
{
	if (player.getPosition() == chicken.getPosition())  // perimetrul gainii si navetei 
	{
		return 1;
	}
	return 0;
}

bool Collision::playerAsteroidColliding(Player& player, Asteroid& asteroid)
{
	if (player.getPosition().getPositionX() <= asteroid.GetPosition().getPositionX() + static_cast <float> (asteroid.GetSize()) 
		&& player.getPosition().getPositionX() + player.getSize() >= asteroid.GetPosition().getPositionX() + static_cast <float> (asteroid.GetSize())
		&& player.getPosition().getPositionY() <= asteroid.GetPosition().getPositionY() + static_cast <float> (asteroid.GetSize())
		&& player.getPosition().getPositionY() + player.getSize() >= asteroid.GetPosition().getPositionY() + static_cast <float> (asteroid.GetSize()))
	{
		return true;
	}
	return false;
}

bool Collision::gunCollidingChicken(Gun& gun, chicken& chicken)
{	
	if ((gun.getPosition().getPositionX() >= chicken.getPosition().getPositionX() && 
		gun.getPosition().getPositionX() <= chicken.getPosition().getPositionX()+chicken.getSize()) && 
		gun.getPosition().getPositionY() <= chicken.getPosition().getPositionY()+chicken.getSize())
	{
		//chicken.setHealth(chicken.getHealth() - static_cast<int>(gun.getDamage()));
		return true;
	}
	return false;
}

bool Collision::gunCollidingAsteroid(Gun& gun, Asteroid& asteroid) 
{
	if ((gun.getPosition().getPositionX() >= asteroid.GetPosition().getPositionX()) &&
		gun.getPosition().getPositionX() <= asteroid.GetPosition().getPositionX() + static_cast<float>(asteroid.GetSize())&&
		gun.getPosition().getPositionY() <= asteroid.GetPosition().getPositionY() + static_cast<float>(asteroid.GetSize()))
	{
		//chicken.setHealth(chicken.getHealth() - static_cast<int>(gun.getDamage()));
		return true;
	}
	return false;
}

bool Collision::giftCollidingPlayer(Gift& gift, Player& player)
{
	if (player.getPosition() ==  gift.getPosition())   // cadoul "loveste" naveta
	{
		return 1;
	}
	return 0;
}

bool Collision::playerCollidingFood(Food& food, Player& player)
{
	if (player.getPosition() == food.getPosition())   //pulpanasul loveste player-ul
	{
		return 1;
	}
	return 0;
}

bool Collision::gunCollidingFrame(Gun& gun)
{
	if (gun.getPosition().getPositionY() == 0.0f)
		return true;
	return false;
}
bool Collision::asteroidCollidingFrameDown(const Asteroid& asteroid, const sf::RenderWindow& window)
{
	if (asteroid.GetPosition().getPositionY() >= window.getSize().y)
		return true;
	else
		return false;
}

bool Collision::chickenCollindingFrameRight(const std::vector<chicken>& chicks, const sf::RenderWindow& window)
{
	int ok=0;
	for (chicken chick : chicks)
	{
		if (chick.getPosition().getPositionX() < window.getSize().x - chick.getSize())
		{	
			ok++;
		}
	}
	if (ok == chicks.size())
		return false;
	else
		return true;
}

bool Collision::chickenCollindingFrameLeft(const std::vector<chicken>& chicks)
{	
	int ok = 0;
	for (chicken chick : chicks)
	{
		if (chick.getPosition().getPositionX() > 0)
		{
			ok++;
		}
	
	}
	if (ok == chicks.size())
		return false;
	else
		return true;
}
bool Collision::asteroidCollindingFrameRight(const Asteroid& asteroids, const sf::RenderWindow& window)
{
	if(asteroids.GetPosition().getPositionX() >= window.getSize().x)
		return false;
	else
		return true;
}
bool Collision::asteroidCollindingFrameLeft(const Asteroid& astro)
{
	if (astro.GetPosition().getPositionX() <= 0)
		return false;
	else
		return true;
}


