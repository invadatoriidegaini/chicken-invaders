#include "Egg.h"
#include "TextureLoad.h"

Egg::Egg()
{
	m_speed = 0;
	m_texture.loadFromFile(EGG);
	sf::Vector2f size(23, 30);
	m_shape.setSize(size);
	m_shape.setPosition(m_position.getPositionX(), m_position.getPositionY());
	m_shape.setTexture(&m_texture);
}

Egg::Egg(Position position,double viteza)
{

	m_speed = viteza;
	m_position = position;
	m_shape.setPosition(position.getPositionX(), position.getPositionY());
}

void Egg::setSpeed(double viteza)
{
	m_speed = viteza;
}

double Egg::getSpeed() const
{
	return m_speed;
}

Position Egg::getPosition() const
{
	return m_position;
}

Egg::~Egg()
{
}

void Egg::move(float x, float y)
{
	m_position.setPositionX(m_position.getPositionX() + x);
	m_position.setPositionY(m_position.getPositionY() + y);
	m_shape.move(x, y);
}

void Egg::drawEgg(sf::RenderTarget& window)
{
	window.draw(m_shape);
}