#pragma once
#include "TextureLoad.h"
#include <SFML/Graphics/Texture.hpp>
class Texture
{
public:
	Texture();
	sf::Texture& getChickenREDTexture();
	sf::Texture& getChickenBlueTexture();
	sf::Texture& genGunTexture();
	sf::Texture& getChickenBLUETexture();
	sf::Texture& getGunTexture();
	sf::Texture& getPlayerTexture();
	sf::Texture& getAsteroidTexture();
	sf::Texture& getEggTexture();
	sf::Texture& getGiftTexture();
	sf::Texture& getBossTexture();

private:
	sf::Texture m_chickenREDTexture;
	sf::Texture m_chickenBLUETexture;
	sf::Texture m_gunTexture;
	sf::Texture m_playerTexture;
	sf::Texture m_asteroidTexture;
	sf::Texture m_eggTexture;
	sf::Texture m_giftTexture;
	sf::Texture m_bossTexture;
};

