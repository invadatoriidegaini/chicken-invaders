#pragma once
class Position
{
public:
	Position();
	Position(float x, float y);
	Position(const Position &position);
	~Position() = default;

	const float getPositionX() const;
	const float getPositionY() const;

	void setPositionX(float x);
	void setPositionY(float y);

	Position& operator=(const Position& position);
	friend bool operator==(const Position&, const Position&);
private:
	float m_x;
	float m_y;
};

