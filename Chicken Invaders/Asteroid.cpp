#include "Asteroid.h"
#include "TextureLoad.h"
const float Asteroid::radius[4] = { 90.0f,60.0f,30.0f,5.0f };
Asteroid::Asteroid()
	:m_health(0),
	m_damage(0),
	m_size(Size::Unknown),
	m_speed(0),
	m_is_alive(true),
	direction(sf::Vector2f(cos(m_angle* DEG2RAD), sin(m_angle* DEG2RAD))),
	m_form(sf::Quads,4)
{
	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX() + radius[3], m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX() + radius[3], m_position.getPositionY() + radius[3]);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY() + radius[3]);

	m_form[0].texCoords = sf::Vector2f(0.f, 0.f);
	m_form[1].texCoords = sf::Vector2f(200.f, 0.f);
	m_form[2].texCoords = sf::Vector2f(200.f, 200.f);
	m_form[3].texCoords = sf::Vector2f(0.f, 200.f);
	
}

Asteroid::Asteroid(uint8_t health, uint8_t damage, Size size,Position position,float speed,float angle,bool is_alive):m_health(health),m_damage(damage),m_size(size),m_position(position),m_speed(speed),m_angle(angle),m_is_alive(is_alive), direction(sf::Vector2f(cos(m_angle * DEG2RAD), sin(m_angle * DEG2RAD))),m_form(sf::Quads,4)
{
	float size_t;
	if (size == Size::GIANT)
		size_t=radius[0];
	else
		if (size == Size::MEDIUM)
			size_t=radius[1];
		else 
			if (size == Size::SMALL)
				size_t=radius[2];
			else
				size_t=radius[3];

	m_form[0].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	m_form[1].position = sf::Vector2f(m_position.getPositionX() + size_t, m_position.getPositionY());
	m_form[2].position = sf::Vector2f(m_position.getPositionX() + size_t, m_position.getPositionY() + size_t);
	m_form[3].position = sf::Vector2f(m_position.getPositionX(), m_position.getPositionY() + size_t);

	m_form[0].texCoords = sf::Vector2f(0.f, 0.f);
	m_form[1].texCoords = sf::Vector2f(126.f, 0.f);
	m_form[2].texCoords = sf::Vector2f(126.f, 126.f);
	m_form[3].texCoords = sf::Vector2f(0.f, 126.f);

}

uint8_t Asteroid::GetHealth() const
{
	return m_health;
}

uint8_t Asteroid::GetDamage() const
{
	return m_damage;
}

Asteroid::Size Asteroid::GetSize() const
{
	return m_size;
}

Position Asteroid::GetPosition() const
{
	return m_position;
}

float Asteroid::GetSpeed() const
{
	return m_speed;
}

float Asteroid::GetAngle() const
{
	return m_angle;
}

void Asteroid::SetHealth(uint8_t health)
{
	this->m_health = health;
}

void Asteroid::SetDamage(uint8_t damage)
{
	this->m_damage = damage;
}

void Asteroid::SetSize(Size size)
{
	this->m_size = size;
}

void Asteroid::SetPosition(Position position)
{
	this->m_position = position;
	m_form[0].position = sf::Vector2f(position.getPositionX(), position.getPositionY());
	m_form[1].position = sf::Vector2f(position.getPositionX() + static_cast <float> (m_size), position.getPositionY());
	m_form[2].position = sf::Vector2f(position.getPositionX() + static_cast <float> (m_size), position.getPositionY() + static_cast <float> (m_size));
	m_form[3].position = sf::Vector2f(position.getPositionX(), position.getPositionY() + static_cast <float> (m_size));
	
}

void Asteroid::SetSpeed(uint8_t speed)
{
	this->m_speed = speed;
}

void Asteroid::SetAngle(float angle)
{
	this->m_angle = angle;
}

void Asteroid::SetDirection(float x,float y)
{
	this->direction = sf::Vector2f(x, y);
	
}


Asteroid::Asteroid(const Asteroid& asteroid)
{
	*this = asteroid;
}

void Asteroid::move()
{
	m_position.setPositionX(m_position.getPositionX() + direction.x * m_speed);
	m_position.setPositionY(m_position.getPositionY() + direction.y * m_speed);

	m_form[0].position += sf::Vector2f(direction.x * m_speed, direction.y * m_speed);
	m_form[1].position += sf::Vector2f(direction.x * m_speed, direction.y * m_speed);
	m_form[2].position += sf::Vector2f(direction.x * m_speed, direction.y * m_speed);
	m_form[3].position += sf::Vector2f(direction.x * m_speed, direction.y * m_speed);
	

}

bool Asteroid::isAlive()
{
	if(m_health!=0)
		return m_is_alive;
}



bool Asteroid::CheckPoint(sf::Vector2f point)
{
	float ax = GetPosition().getPositionX();
	float ay = GetPosition().getPositionY();
	float px = point.x;
	float py = point.y;
	float sqrDistance = ((ax - px) * (ax - px) + (ay - py) * (ay - py));
	float sqrRadius = radius[1] * radius[1];
	return (sqrDistance <= sqrRadius);
}

void Asteroid::Update()
{
	if (m_form[0].texCoords.x == 889)
	{
		m_form[0].texCoords.x = 0;
		if (m_form[0].texCoords.y == 882)
		{
			m_form[0].texCoords.y = 0;
		}
		else
		{
			m_form[0].texCoords.y += 126;
		}
	}
	else
	{
		m_form[0].texCoords.x += 127;
	}
	/*m_form[1].texCoords += sf::Vector2f(126.f, 0.f);
	m_form[2].texCoords += sf::Vector2f(126.f, 126.f);
	m_form[3].texCoords += sf::Vector2f(0.f, 126.f);*/
}

void Asteroid::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_form, states);
}





std::ostream& operator<<(std::ostream& out, const Asteroid& asteroid)
{
	out << static_cast<int>(asteroid.m_health) << static_cast<int>(asteroid.m_damage) << static_cast<int>(asteroid.m_size);
	return out;
}
