#pragma once
#include<time.h>
#include <cstdlib>
#include "Chicken.h"
#include "Position.h"
class Gift
{
public:

	bool generare(); //functie de generare random a cadoului
	Gift(Position position);
	Position getPosition() const;
	void move(float x, float y);
	

private:
	Position m_position;
	
};

