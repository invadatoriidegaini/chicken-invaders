#include "SoundManager.h"

void SoundManager::LoadSounds() 
{

	if (!m_backgroundMusic.openFromFile(SONG1))
	{
		throw std::exception("Background Music couldn't be opened");
	}
	
}

void SoundManager::InitializeMusic() {
	m_backgroundMusic.setVolume(5);
	m_backgroundMusic.setLoop(true);
}

SoundManager::SoundManager()
{
	LoadSounds();
	InitializeMusic();
}

SoundManager::~SoundManager()
{
}


void SoundManager::PlayMusic()
{
	m_backgroundMusic.play();
}


void SoundManager::StopMusic()
{
	m_backgroundMusic.stop();
}