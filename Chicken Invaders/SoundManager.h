#pragma once
#include <SFML/Audio.hpp>
#include "TextureLoad.h"
	class SoundManager
	{
	public:
		void LoadSounds();
		void InitializeMusic();
		SoundManager();
		~SoundManager();

		void PlayMusic();
		void StopMusic();

	private:
		
		sf::Music m_backgroundMusic;
	};

