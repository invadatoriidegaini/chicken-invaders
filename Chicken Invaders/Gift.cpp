#include "Gift.h"

bool Gift::generare()
{
	srand(time(0));

	if (7==1+rand()%10)
	{
		return 1;
	}
	return 0;
}
Gift::Gift(Position position):m_position(position)
{
}
Position Gift::getPosition() const
{
	return m_position;
}

void Gift::move(float x, float y)
{
	m_position.setPositionX(m_position.getPositionX() + x);
	m_position.setPositionY(m_position.getPositionY() + y);
}
