#pragma once
#include <vector>
#include "Egg.h"
#include "Asteroid.h"
#include "Gun.h"
#include "Food.h"
#include "Gift.h"
#include "Player.h"
#include "Rockets.h"
#include "InvaderManager.h"
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"


class Wave
{
public:
	Wave();
	//SETTERS

	void setWaveNumber(const uint16_t&);

	void setGameIsOver(); 


	//GETTERS

	uint16_t getWaveNumber() const;


	//METHODS
	const Player& getPlayer() const;

	bool isGameOver() const;

	int CreateWaveType();


	void playerShooting(Player player);

	std::vector<Asteroid>& getAtros();
	void createWave(const int&);
	void moveWave(const int&, const sf::RenderWindow&);
	//OPERATORS

	Wave& operator++();
public:
	std::vector <Asteroid> m_atros;

private:
	std::vector <Weapon> m_weapon;
	sf::Clock m_clock;
	bool m_isGameOver = false;
	uint8_t m_wave_type;
	uint16_t m_wave_number;
	
};