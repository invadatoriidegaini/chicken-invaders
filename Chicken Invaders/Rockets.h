#pragma once
#include <iostream>
#include "Weapon.h"
#include "Position.h"
#include <SFML\Graphics.hpp>

class Rockets : public Weapon
{
public:
	Rockets();
	Rockets(uint8_t damage, uint8_t nrRockets,Position position);
	//GETTERS
	uint8_t getDamage() override;
	Position getPosition();
	float getSpeed();
	uint8_t getNumber();
	//SETTERS
	void setDamage(uint8_t dmg) override; 
	void setPosition(Position);
	void setNumber(uint8_t nrRockets);
	//OPERATORS
	Rockets& operator =(const Rockets& rockets);
	Rockets& operator --();
	//METHODS
	void move(float x, float y);
	void update(float frametime);
	void drawRocket(sf::RenderTarget& target);

public:
	sf::Texture m_texture;
	sf::CircleShape m_shape;
private:
	Position m_position;
	uint8_t m_damage;

	const uint8_t m_maxRockets =10;
	uint8_t m_nrRockets;
	static const float m_speed;
	sf::Vector2f direction;
};

