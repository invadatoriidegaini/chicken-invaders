#include "Rockets.h"
#include "TextureLoad.h"

const float Rockets::m_speed = 0.9f;
Rockets::Rockets()
{
	m_damage = UINT8_MAX;
	m_nrRockets = 0;
	setPosition(m_position);
	m_texture.loadFromFile(MISSILE);
	m_shape.setRadius(100);
	m_shape.setPosition(m_position.getPositionX(), m_position.getPositionY());
	m_shape.setTexture(&m_texture);
}

Rockets::Rockets(uint8_t damage, uint8_t nrRockets,Position position):m_damage(damage),m_nrRockets(nrRockets),m_position(position)
{
	m_texture.loadFromFile(MISSILE);
	m_shape.setRadius(100);
	m_shape.setPosition(m_position.getPositionX(), m_position.getPositionY());
	m_shape.setTexture(&m_texture);
}

uint8_t Rockets::getDamage()
{
	return m_damage;
}

Position Rockets::getPosition()
{
	return m_position;
}

float Rockets::getSpeed()
{
	return m_speed;
}

uint8_t Rockets::getNumber()
{
	return m_nrRockets;
}

void Rockets::setPosition(Position position)
{
	m_position = position;
	m_shape.setPosition(position.getPositionX(), position.getPositionY());
}
void Rockets::setNumber(uint8_t nrRockets)
{
	this->m_nrRockets = nrRockets;
}
void Rockets::setDamage(uint8_t dmg)
{
	this->m_damage = dmg;
}


Rockets& Rockets::operator=(const Rockets& rockets)
{
	this->m_damage = rockets.m_damage;
	this->m_nrRockets = rockets.m_nrRockets;
	return *this;
}

Rockets& Rockets::operator--()
{
	this->m_nrRockets -= 1;
	return *this;
}

void Rockets::move(float x, float y)
{
	m_position.setPositionX(m_position.getPositionX() + x);
	m_position.setPositionY(m_position.getPositionY() + y);
	m_shape.move(x, y);
}

void Rockets::update(float frametime)
{
	sf::Vector2f distance = direction * m_speed * frametime;
	std::move(distance);
}

void Rockets::drawRocket(sf::RenderTarget& target)
{
	target.draw(m_shape);
}
