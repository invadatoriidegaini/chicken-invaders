#include <iostream>
#include "Boss.h"

// CONSTRUCTORS AND DESTRUCTORS

Boss::Boss()
	:m_health(0),
	m_position(0, 0),
	m_is_alive(true)
{
	m_sprite_boss.setPosition(m_position.getPositionX(), m_position.getPositionY());
	m_sprite_boss.setScale(0.9, 0.6);
};

Boss::Boss(Position position, int health, bool is_alive)
	:m_position(position),
	m_health(health),
	m_is_alive(is_alive)
{
	m_sprite_boss.setPosition(m_position.getPositionX(), m_position.getPositionY());
	m_sprite_boss.setScale(0.9, 0.6);
}

Boss::Boss(const Boss& boss)
	:m_health(boss.m_health),
	m_is_alive(boss.m_is_alive),
	m_position(boss.m_position),
	m_texture_boss(boss.m_texture_boss),
	m_sprite_boss(boss.m_sprite_boss)
{

}

Boss::~Boss()
{}

//GETTERS

Position Boss::getPosition() const
{
	return m_position;
}

uint8_t Boss::getHealth() const
{
	return m_health;
}

int Boss::getSpeed() const
{
	return this->m_speed;
}

//SETTERS

void Boss::setHealth(int health)
{
	m_health -= health;
	std::cout << "Collision working!\n";
}

void Boss::setPosition(Position position)
{
	m_position = position;
	m_sprite_boss.setPosition(position.getPositionX(), position.getPositionY());
}

void Boss::setSpeed(int speed)
{
	m_speed = speed;
}

bool Boss::isAlive()
{
	if (m_health != 0)
		return m_is_alive;
}

//OTHER FUNCTIONS

void Boss::Attack()
{
	while (isAlive())
	{
		srand(time(0));

		if (5 == 1 + rand() % 5)

			Egg(getPosition(), 2);

	}
}
void Boss::move(float x, float y)
{
	m_position.setPositionX(m_position.getPositionX() + x);
	m_position.setPositionY(m_position.getPositionY() + y);
	m_sprite_boss.move(x, y);
}

bool Boss::textureLoadBoss1()
{
	if (!m_texture_boss.loadFromFile(BOSS1))
	{
		std::cout << "Texture for Boss1 couldn't be found!\n";
		return 0;
	}
	std::cout << "Texture loaded successfully!\n";
	return 1;

}

bool Boss::textureLoadBoss2()
{

	if (!m_texture_boss.loadFromFile(BOSS2))
	{
		std::cout << "Texture for Boss2 couldn't be found!\n";
		return 0;
	}
	std::cout << "Texture loaded successfully!\n";
	return 1;

}
void Boss::setTextureBoss1()
{
	if (textureLoadBoss1())
	{
		m_sprite_boss.setTexture(m_texture_boss);
	}
	else
	{
		std::cout << "Texture not loaded!\n";
	}
}

void Boss::setTextureBoss2() 
{
	if (textureLoadBoss2())
	{
		m_sprite_boss.setTexture(m_texture_boss);
	}
	else
	{
		std::cout << "Texture not loaded!\n";
	}

}
void Boss::move(float x, float y)
{
	m_position.setPositionX(m_position.getPositionX() + x);
	m_position.setPositionY(m_position.getPositionY() + y);

	m_sprite_boss.setPosition(x, y);
}

void Boss::drawBoss(sf::RenderWindow& window)
{
	window.draw(m_sprite_boss);
}

void Boss::generatePath(sf::Vector2f pos) noexcept
{
	sf::Vector2f aimDir;
	sf::Vector2f aimDirNorm;

	//pos.x = rand() % 1300;
	//pos.y = rand() % 400;

	//Update
	aimDir = pos - sf::Vector2f(m_position.getPositionX(), m_position.getPositionY());
	aimDirNorm = aimDir / sqrt(pow(aimDir.x, 2) + pow(aimDir.y, 2));

	m_velocity = sf::Vector2f(aimDirNorm.x * m_speed, aimDirNorm.y * m_speed);

	m_position.setPositionX(m_velocity.x);
	m_position.setPositionY(m_velocity.y);
}

Boss& Boss::operator=(const Boss& boss)
{
	if (this != &boss)
	{
		return *this;
	}
	this->m_health = boss.m_health;
	this->m_is_alive = boss.m_is_alive;
	this->m_position = boss.m_position;
	return *this;
}


