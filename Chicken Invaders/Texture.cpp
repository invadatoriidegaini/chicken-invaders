#include "Texture.h"

Texture::Texture()
{
	this->m_chickenREDTexture.loadFromFile(CHICKENRED);
	this->m_chickenBLUETexture.loadFromFile(CHICKENBLUE);
	this->m_gunTexture.loadFromFile(GUN);
	this->m_playerTexture.loadFromFile(SHIP);
	this->m_asteroidTexture.loadFromFile(ASTEROID);
	this->m_eggTexture.loadFromFile(EGG);
	this->m_giftTexture.loadFromFile(GIFT);
	this->m_bossTexture.loadFromFile(BOSS1);
}

sf::Texture& Texture::getChickenREDTexture()
{
	return m_chickenREDTexture;
}

sf::Texture& Texture::getChickenBLUETexture()
{
	return m_chickenBLUETexture;
}

sf::Texture& Texture::getGunTexture()
{
	return m_gunTexture;
}

sf::Texture& Texture::getPlayerTexture()
{
	return m_playerTexture;
}

sf::Texture& Texture::getAsteroidTexture()
{
	return m_asteroidTexture;
}

sf::Texture& Texture::getEggTexture()
{
	return m_eggTexture;
}

sf::Texture& Texture::getGiftTexture()
{
	return m_giftTexture;
}

sf::Texture& Texture::getBossTexture()
{
	return m_bossTexture;
}
