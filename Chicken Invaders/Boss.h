#pragma once
#include<time.h>
#include <cstdlib>
#include <iostream>
#include "Position.h"
#include "Egg.h"
#include "TextureLoad.h"
#include "SFML/Graphics.hpp"

class Boss {

public:

	//CONSTRUCTORS AND DESTRUCTOR

	Boss();

	Boss(Position pos, int hp, bool is_alive);

	Boss(const Boss&);

	~Boss();

	//GETTERS

	Position getPosition() const;
	uint8_t getHealth() const;
	int getSpeed() const;

	//SETTERS

	void setHealth(int);
	void setPosition(Position pos);
	void setSpeed(int);

	//OTHER FUNCTIONS

public:

	bool isAlive();
	void Attack();
	sf::Vector2f Interpolate(const sf::Vector2<float>&, const sf::Vector2<float>&, float);
	bool textureLoadBoss1();
	bool textureLoadBoss2();
	void setTextureBoss1();
	void setTextureBoss2();
	void drawBoss(sf::RenderWindow&);
	void generatePath(const sf::Vector2f&);
	void move(float, float);

	//OPERATORS
	Boss& operator=(const Boss&);

private:


	int m_speed;

	int m_health;

	Position m_position;

	bool m_is_alive;

	sf::Texture m_texture_boss;

	sf::Vector2f m_velocity;

	sf::Sprite m_sprite_boss;



};