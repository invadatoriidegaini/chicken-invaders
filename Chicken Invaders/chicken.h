#pragma once
#include<time.h>
#include <cstdlib>
#include <iostream>
#include "Position.h"
#include "Egg.h"
#include "TextureLoad.h"

class chicken: public sf::Drawable 
{

public:

	//CONSTRUCTORS AND DESTRUCTOR

	chicken();

	chicken(Position pos, int hp, bool is_alive);

	chicken(const chicken&);

	~chicken();

	//GETTERS

	int getSpeed() const;
	Position getPosition() const;
	uint8_t getHealth() const;
   	sf::VertexArray getChickenDrawablecontent();
	int getSize() const;

	//SETTERS
	void setSpeed(int);
	void setHealth(uint8_t);
	void setPosition(Position );
	void setSize(int);

	

	//OTHER FUNCTIONS
	void Attack();
	bool isAlive();
	void move(float x, float y);
	
	

private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:

	int m_speed;

	uint8_t m_health;

	int m_size;

	Position m_position;

	bool m_is_alive;

	sf::Texture m_chicken_texture;
	
	sf::VertexArray m_form;
};