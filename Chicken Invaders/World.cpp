#include "World.h"
#include "Position.h"
#include "Collision.h"

//CONSTRUCTORS AND DESTRUCTORS

World::World()
	:m_wave()
{
	{
		this->m_font.loadFromFile(FONT);
		this->m_text.setString("GAME OVER");
		this->m_text.setCharacterSize(124);
		this->m_text.setFont(m_font);
		this->m_text.setStyle(sf::Text::Italic);
		this->m_text.setFillColor(sf::Color::Red);
	}

	this->initializeVariables();
	this->initWindow();

	this->m_background = new MovableBackground(*m_window);

	this->m_player.setLife(static_cast<int8_t>(3));
	this->m_player.setPosition(Position(m_window->getSize().x / 2, m_window->getSize().y - 70));
	this->m_player.setSpeed(10);

	gameStart(*this);
}

World::~World()
{
	delete this->m_window;
}

//PRIVATE FUNCTIONS
void World::initializeVariables()
{
	this->m_window = nullptr;
}

void World::initWindow()
{
	this->m_window = new sf::RenderWindow(sf::VideoMode(this->m_videomode), "Chicken Invaders", sf::Style::Fullscreen);
}

sf::RenderWindow* World::getWindow()
{
	return m_window;
}

//GETTERS AND SETTERS
void World::setHeightResolution(const int& height)
{
	this->m_height = height;
}

void World::setWidthResolution(const int& width)
{
this->m_width = width;
}

int World::getHeightResolution() const
{
	return m_height;
}

int World::getWidhtResolution() const
{
	return m_width;
}

//ACCESORS FUNCTION
const bool World::running() const
{
	return this->m_window->isOpen();
}



//METHODS

void World::updateEvents()
{
	//Event updating
	while (m_window->pollEvent(this->m_ev))
	{
		switch (this->m_ev.type)
		{
		case sf::Event::Closed:
		{
			this->m_window->close();
			break;
		}
		case sf::Event::KeyReleased:
		{
			m_player.is_fire_gun = false;
			break;
		}
		case sf::Event::KeyPressed:
		{
			if (this->m_ev.key.code == sf::Keyboard::Escape)
			{
				this->m_window->close();
			}

			if (this->m_ev.key.code == sf::Keyboard::Left && m_player.getPosition().getPositionX() > 10 )
			{
				this->m_player.move(-this->m_player.getSpeed(), 0.f);
			}

			if (this->m_ev.key.code == sf::Keyboard::Right && m_player.getPosition().getPositionX() < m_window->getSize().x - m_player.getSize())
			{
				this->m_player.move(this->m_player.getSpeed(), 0.f);
			}

			if (this->m_ev.key.code == sf::Keyboard::Space && m_player.getLife() > 0 && !m_player.is_fire_gun)
			{
				this->m_player.getGun().setPosition(Position(m_player.getPosition().getPositionX() + 27.3, m_player.getPosition().getPositionY()));
				this->m_player.getGun().setSpeed(1);

				bullets.push_back(Gun(m_player.getGun()));
				m_player.is_fire_gun = true;
			}

			break;
		}
		}
	}
}
void World::update()
{
	this->m_background->Update(*m_window, 0.2);
	this->updateEvents();
}

void World::render()
{
	this->m_background->Render(*m_window);
	if (m_player.getLife() > static_cast <int8_t>(0))
	{

		if (InvaderManager::m_asteroids.empty() && InvaderManager::m_chickens.empty())
			m_wave.createWave(m_wave.CreateWaveType());
		else
		{
			if (static_cast<int>(m_wave.getWaveNumber()) < 3)
			{
				InvaderManager::drawChickens(*m_window, m_texture);
				m_wave.moveWave(static_cast<int>(m_wave.getWaveNumber()), *m_window);
			}
			else
			{
				InvaderManager::drawAsteroid(*m_window, m_texture);
				m_wave.moveWave(static_cast<int>(m_wave.getWaveNumber()), *m_window);
			}
		}

		for (size_t i = 0; i < bullets.size(); i++)
		{

			this->m_window->draw(bullets.at(i), &m_texture.getGunTexture());

			bullets.at(i).move(0.f, -bullets.at(i).getSpeed());

			if (Collision::gunCollidingFrame(bullets.at(i)))
			{
				bullets.erase(bullets.begin() + i);
				break;
			}
			else
			{
				if (!InvaderManager::m_chickens.empty())
				{
					for (std::vector<chicken>::iterator it = InvaderManager::m_chickens.begin(); it != InvaderManager::m_chickens.end(); ++it)
					{
						if (Collision::gunCollidingChicken(bullets[i], *it))
						{
							bullets.erase(bullets.begin() + i);
							InvaderManager::m_chickens.erase(it);
							m_player.setScore(m_player.getScore() + 50);
							break;
						}
					}
				}
				if (!InvaderManager::m_asteroids.empty())
				{
					for (std::vector<Asteroid>::iterator it = InvaderManager::m_asteroids.begin(); it != InvaderManager::m_asteroids.end(); ++it)
					{
						if (Collision::gunCollidingAsteroid(bullets[i], *it))
						{
							bullets.erase(bullets.begin() + i);
							InvaderManager::m_asteroids.erase(it);
							m_player.setScore(m_player.getScore() + 50);
							break;
						}

					}
				}
			}

		}

		for (std::vector<Asteroid>::iterator it = InvaderManager::m_asteroids.begin(); it != InvaderManager::m_asteroids.end(); ++it)
		{
			if (Collision::playerAsteroidColliding(m_player, *it) && m_player.getLife() > static_cast <int8_t> (0))
			{
				m_player.setLife(m_player.getLife() - static_cast <int8_t>(1));
				m_player.setPosition(Position(m_window->getSize().x / 2, m_window->getSize().y - 70));
				InvaderManager::m_asteroids.erase(it);
				break;
			}
		}

		this->m_window->draw(m_player, &m_texture.getPlayerTexture());
	}
	else
	{
		m_text.setPosition(sf::Vector2f((m_window->getSize().x / 2) - 300, (m_window->getSize().y / 2) - 124));
		this->m_window->draw(m_text);
	}

	//Draw game objects
	this->m_window->display();
}

void World::gameStart(World& world)
{
	while (world.running())
	{
		//Update
		world.update();
		//Render
		world.render();

	}
}
