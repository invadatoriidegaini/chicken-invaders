#include "Wave.h"
#include "Collision.h"

Wave::Wave()
{
	this->m_wave_type = 0;
	this->m_wave_number = 0;
}

uint16_t Wave::getWaveNumber() const
{
	return this->m_wave_number;
}





void Wave::setWaveNumber(const uint16_t& waveNo)
{
	this->m_wave_number = waveNo;
}

void Wave::setGameIsOver()
{
	this->m_isGameOver = true;
}


int Wave::CreateWaveType() //There are 7 different types of waves that we want to implement: 3 for the Chickens, 3 for the Asteroids and one for the Boss
{
	this->m_wave_type = (m_wave_type + 1) % 4;
	this->m_wave_number = m_wave_type;
	return static_cast<int>(m_wave_type);
}

std::vector<Asteroid>& Wave::getAtros()
{
	return m_atros;
}

void Wave::createWave(const int& waveType)
{
	switch (waveType) 
	{

	//Five rows of eight Chickens appear

	case 1: 
	{
		InvaderManager::m_chickens = std::vector <chicken>(0);
		InvaderManager::m_chickens.clear();
		int x = 200;
		int y = 25;
		int contor = 0;
		while (contor < 32)
		{
			if (contor % 8 == 0 && contor != 0)
			{
				y += 100;
				x = 200;
			}

			Position chicken_pos(x, y);
			chicken new_chicken(chicken_pos, 1, true);
			InvaderManager::insertChicken(new_chicken);
			contor++;
			x += 125;
		}
		break;
	}
	
	/*Five Chickens fly around from left to right, 
	  each time coming closer to the bottom, and will eventually crash into you if 
	  you don't defeat them fast enough. Will spawn another 1 at every new chapter(10 waves).*/
	
	case 2:
	{
		InvaderManager::m_chickens = std::vector <chicken>(0);
		InvaderManager::m_chickens.clear();
		int x = 375;
		int y = 25;
		for (int index = 0; index < 5+getWaveNumber()%10; index++)
		{
			Position chicken_pos(x, y);
			chicken new_chicken(chicken_pos, 1, 1);
			InvaderManager::insertChicken(new_chicken);
			x += 150;
		}
		// de adaugat move-ul catre partea de jos a ecranului
		break;
	}
	
	/*Lines of eight Chickens fly from the top of the screen downwards alternatingly to the left, then to the right.
	6 lines of them spawn.They also fly faster every system.
	This wave is also notable in that it's the only one where a Missile won't kill all of the Chickens in the wave, since they don't spawn all at once.*/
	
	case 10: 
	{
		InvaderManager::m_chickens = std::vector <chicken>(0);
		InvaderManager::m_chickens.clear();
		int x = 375;
		int y = 25;
		int contor = 0;
		while(contor < 40)
		{
			if (contor % 8 == 0 && contor != 0)
			{
				y += 90;
				x = 375;
		
			}
			
			Position chicken_pos(x, y);
			chicken new_chicken(chicken_pos, 1, 1);
			//new_chicken.setTexture();
			InvaderManager::insertChicken(new_chicken);
			contor++;
			x += 150;
		}
	}

	/*30 Asteroids fall from the top of the screen, and you have to avoid or destroy them.*/

	case 3: // resize the vector you should do
	{	
		InvaderManager::m_asteroids = std::vector<Asteroid>(0);
		InvaderManager::m_asteroids.clear();
		for (int index = 0; index < 50; index++)
		{	
			Position pos(rand() % 1500, -(rand() % 400));
			switch (rand() % 3 + 1)
			{
			case 1:
			{
				Asteroid asteroid(3, 1,Asteroid::Size::GIANT , pos, 0.3f,90, true);
				InvaderManager::insertAsteroid(asteroid);
				break;
			}
			case 2:
			{
				Asteroid asteroid(3, 1, Asteroid::Size::MEDIUM, pos, 0.4f, 90, true);
				InvaderManager::insertAsteroid(asteroid);
				break;
			}
			case 3:
			{
				Asteroid asteroid(3, 1, Asteroid::Size::SMALL, pos, 0.5f, 90, true);
				InvaderManager::insertAsteroid(asteroid);
				break;
			}
			case 4:
			{
				Asteroid asteroid(3, 1, Asteroid::Size::Unknown, pos, 0.5f, 90, true);
				InvaderManager::insertAsteroid(asteroid);
				break;
			}
			default:
				break;
			}
			
		}

		break;
	}

	/*20 Asteroids fall from the right of the screen, and you have to avoid or destroy them.*/
	
	case 5: 
	{
		break;
	}

	/*20 Asteroids fall from the left of the screen, and you have to avoid or destroy them.*/
	
	case 6: 
	{
		break;
	}

	/*A boss fight with a Big Chicken who flies around the screen and constantly drops eggs.*/
	
	case 7: 
	{
		break;
	}

	}
}
int comutator = 1;
void Wave::moveWave(const int& waveType, const sf::RenderWindow& window)
{
	switch (waveType)
	{
	case 1:
	{
		if (comutator == 1)
			InvaderManager::MoveChickens(Position(0.3, 0));
		else
			InvaderManager::MoveChickens(Position(-0.3, 0));
		if (Collision::chickenCollindingFrameLeft(InvaderManager::m_chickens)
			|| Collision::chickenCollindingFrameRight(InvaderManager::m_chickens, window))
		{
			comutator = comutator * -1;
		}
		break;
	}
	case 2:
	{	

		break;
	}
	case  10:
	{
		break;
	}
	case 3:
	{
		Position pos(0, 1);
		InvaderManager::MoveAsteroids(pos,window);

		for (std::vector<Asteroid>::iterator it = InvaderManager::m_asteroids.begin(); it != InvaderManager::m_asteroids.end(); ++it)
		{
			if (Collision::asteroidCollidingFrameDown(*it, window)) //|| Collision::asteroidCollindingFrameRight(*it, window))
			{
				InvaderManager::m_asteroids.erase(it);
				break;
			}
		}
		break;
	}
	default:
	{
		break;
	}
	}
}

Wave& Wave::operator++()
{
	this->m_wave_number++;
	return *this;
}