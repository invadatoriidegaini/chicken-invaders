#include "StateBase.h"
#include "Game.h"

StateBase::StateBase(Game& game, const char* name)
	:m_pGame(&game)
	,m_name(name)
{

}

StateBase::StateBase(Game& game,const char* name, unsigned rezizeWindowWidth, unsigned resizeWindowHeight)
	:m_pGame(&game)
	,m_name(name)
{
	m_pGame->resizeWindow(rezizeWindowWidth, resizeWindowHeight);
}
