#include "Logger.h"

Logger::Logger(std::ostream& os) :
	os(os)
{
	// Empty
}

const char* LogLevelToString(Logger::Level level)
{
	switch (level)
	{
	case Logger::Level::Info:
		return "Info";
	case Logger::Level::Warning:
		return "Warning";
	case Logger::Level::Error:
		return "Error";
	default:
		return "";
	}
}

void Logger::log(const std::string& message, Level level)
{
	os << message;
	std::time_t crtTime = std::time(nullptr);
	os << '[' << std::put_time(std::localtime(&crtTime), "%Y-%m-%d %H:%M:%X") << ']';
}
