#pragma once
#pragma warning(disable : 4996) 
#include <cstdint>
#include <iostream>
#include <ostream>
#include <iomanip>
#include <ctime>
#include <mutex>

#include <fstream>

#ifdef LOGGING_EXPORTS
#define API_LOGGER __declspec(dllexport)
#else
#define API_LOGGER __declspec(dllimport)
#endif

class Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};

public:
	Logger(std::ostream& os);

	void log(const std::string& message, Level level);

private:
	std::ostream& os;
	Level minimumLevel;
};