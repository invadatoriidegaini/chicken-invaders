#include "pch.h"
#include "CppUnitTest.h"
#include "../Chicken Invaders/Position.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChickenInvadersTests
{
	TEST_CLASS(PositionTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			Position pos(0, 0);
			Assert::IsTrue(pos.getPositionX() == 0);
			Assert::IsTrue(pos.getPositionY() == 0);
		}
		TEST_METHOD(GetPositionX)
		{
			Position pos(0, 0);
			Assert::IsTrue(pos.getPositionX() == 0);
		}
		TEST_METHOD(GetPositionY)
		{
			Position pos(0, 0);
			Assert::IsTrue(pos.getPositionY() == 0);
		}
		TEST_METHOD(CopyConstructor)
		{
			Position pos1(0, 0);
			Position pos2 = pos1;
			Assert::IsTrue(pos1.getPositionX() == pos2.getPositionX());
			Assert::IsTrue(pos1.getPositionY() == pos2.getPositionY());
		}
		TEST_METHOD(OperatorEqual)
		{
			Position pos1(0, 0);
			Position pos2(pos1);
			Assert::IsTrue(pos1.getPositionX() == pos2.getPositionX());
			Assert::IsTrue(pos1.getPositionY() == pos2.getPositionY());
		}
	};
}