#include "pch.h"
#include "CppUnitTest.h"
#include "../Chicken Invaders/Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChickenInvadersTests
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(Constructor)
		{	
			Position pos(0, 0);
			Rockets rockets(0,0,Position(0,0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player(pos,3,rockets,gun,food,0,0);
			Assert::IsTrue(player.getPosition() == pos);
			Assert::IsTrue(player.getLife() == 3);
			Assert::IsTrue(player.getRockets().getNumber() == rockets.getNumber());
			Assert::IsTrue(player.getRockets().getDamage() == rockets.getDamage());
			Assert::IsTrue(player.getRockets().getPosition() == rockets.getPosition());
			Assert::IsTrue(player.getGun().getDamage() == gun.getDamage());
			Assert::IsTrue(player.getGun().getlevel() == gun.getlevel());
			Assert::IsTrue(player.getGun().getSpeed() == gun.getSpeed());
			Assert::IsTrue(player.getGun().getPosition() == Position(0,0));
			Assert::IsTrue(player.getQuantity().getQuantity() == food.getQuantity());
			Assert::IsTrue(player.getQuantity().getSpeed() == food.getSpeed());
			Assert::IsTrue(player.getQuantity().getPosition() == food.getPosition());
			Assert::IsTrue(player.getScore() == 0);
			Assert::IsTrue(player.getSpeed() == 0);

		}
		TEST_METHOD(GetPosition)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0,Position(0,0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player(pos, 3, rockets, gun, food, 0, 0);
			Assert::IsTrue(player.getPosition() == pos);
		}
		TEST_METHOD(GetLife)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0,Position(0,0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player(pos, 3, rockets, gun, food, 0, 0);
			Assert::IsTrue(player.getLife() == 3);
		}
		TEST_METHOD(GetRockets)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0,Position(0,0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player(pos, 3, rockets, gun, food, 0, 0);
			Assert::IsTrue(player.getRockets().getNumber() == rockets.getNumber());
			Assert::IsTrue(player.getRockets().getDamage() == rockets.getDamage());
			Assert::IsTrue(player.getRockets().getPosition() == rockets.getPosition());
		}
		TEST_METHOD(GetGun)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0,Position(0,0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player(pos, 3, rockets, gun, food, 0, 0);
			Assert::IsTrue(player.getGun().getDamage() == gun.getDamage());
			Assert::IsTrue(player.getGun().getlevel() == gun.getlevel());
			Assert::IsTrue(player.getGun().getPosition() == gun.getPosition());
			Assert::IsTrue(player.getGun().getSpeed() == gun.getSpeed());
		}
		TEST_METHOD(GetFood)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0,Position(0,0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player(pos, 3, rockets, gun, food, 0, 0);
			Assert::IsTrue(player.getQuantity().getQuantity() == food.getQuantity());
			Assert::IsTrue(player.getQuantity().getSpeed() == food.getSpeed());
			Assert::IsTrue(player.getQuantity().getPosition() == food.getPosition());
		}
		TEST_METHOD(GetScore)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0,Position(0,0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player(pos, 3, rockets, gun, food, 0, 0);
			Assert::IsTrue(player.getScore() == 0);
		}
		TEST_METHOD(Move)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0, Position(0, 0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player(pos, 3, rockets, gun, food, 0, 0);
			player.move(1, 1);
			Assert::IsTrue(player.getPosition() == Position(1, 1));
		}
		TEST_METHOD(SetFireGun)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0, Position(0, 0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player(pos, 3, rockets, gun, food, 0, 0);
			player.setFireGun(true);
			Assert::IsTrue(player.is_fire_gun == true);
		}
		TEST_METHOD(SetFireRocket)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0, Position(0, 0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player(pos, 3, rockets, gun, food, 0, 0);
			player.setFireRocket(true);
			Assert::IsTrue(player.is_fire_rocket == true);
		}
		TEST_METHOD(OperatorEqual)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0, Position(0, 0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player1(pos, 3, rockets, gun, food, 0, 0);
			Player player2(player1);
			Assert::IsTrue(player1.getScore() == player2.getScore());
			Assert::IsTrue(player1.getPosition() == player2.getPosition());
		}
		TEST_METHOD(CopyConstructor)
		{
			Position pos(0, 0);
			Rockets rockets(0, 0, Position(0, 0));
			Gun gun(1, 1, 1, Position(0, 0));
			Food food(0, 0, Position(0, 0));
			Player player1(pos, 3, rockets, gun, food, 0, 0);
			Player player2 = player1;
			Assert::IsTrue(player1.getScore() == player2.getScore());
		}
	};
}