#include "pch.h"
#include "CppUnitTest.h"
#include "../Chicken Invaders/Egg.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChickenInvadersTests
{
	TEST_CLASS(EggTests)
	{
	public:
		TEST_METHOD(Constructor)
		{
			Egg egg(Position(0,0),1);
			Assert::IsTrue(egg.getPosition() == Position(0, 0));
			Assert::IsTrue(egg.getSpeed() == 1);
		}
		TEST_METHOD(GetPosition)
		{
			Egg egg(Position(0, 0), 1);
			Assert::IsTrue(egg.getPosition() == Position(0, 0));
		}
		TEST_METHOD(GetViteza)
		{
			Egg egg(Position(0, 0), 1);
			Assert::IsTrue(egg.getSpeed() == 1);
		}
		TEST_METHOD(Move)
		{
			Egg egg(Position(0, 0), 1);
			egg.move(1, 1);
			Assert::IsTrue(egg.getPosition() == Position(1, 1));
		}
	};
}