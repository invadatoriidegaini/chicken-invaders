#include "pch.h"
#include "CppUnitTest.h"
#include "../Chicken Invaders/Food.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChickenInvadersTests
{
	TEST_CLASS(FoodTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			Food food(0,0, Position(0,0));
			Assert::IsTrue(food.getQuantity() == 0);
			Assert::IsTrue(food.getSpeed() == 0);
			Assert::IsTrue(food.getPosition() == Position(0, 0));
		}
		TEST_METHOD(GetQuantity)
		{
			Food food(0, 0, Position(0, 0));
			Assert::IsTrue(food.getQuantity() == 0);
		}
		TEST_METHOD(GetSpeed)
		{
			Food food(0, 0, Position(0, 0));
			Assert::IsTrue(food.getSpeed() == 0);
		}
		TEST_METHOD(GetPosition)
		{
			Food food(0, 0, Position(0, 0));
			Assert::IsTrue(food.getPosition() == Position(0, 0));
		}
		TEST_METHOD(OperatorPlusPlus)
		{
			Food food(0, 0, Position(0, 0));
			food.operator++();
			Assert::IsTrue(food.getQuantity() == 1);
		}
	};
}