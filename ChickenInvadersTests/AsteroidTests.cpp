#include "pch.h"
#include "CppUnitTest.h"
#include "../Chicken Invaders/Asteroid.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChickenInvadersTests
{
	TEST_CLASS(AsteroidTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			Asteroid asteroid(5, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Assert::IsTrue(asteroid.GetHealth() == 5);
			Assert::IsTrue(asteroid.GetDamage() == 0);
			Assert::IsTrue(Asteroid::Size::GIANT == asteroid.GetSize());
			Assert::IsTrue(asteroid.GetPosition() == Position(0, 0));
			Assert::IsTrue(asteroid.GetSpeed() == 0);
			Assert::IsTrue(asteroid.GetAngle() == 0);
			Assert::IsTrue(asteroid.isAlive() == true);

		}
		TEST_METHOD(GetHealth)
		{
			Asteroid asteroid(0, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Assert::IsTrue(asteroid.GetHealth() == 0);
		}
		TEST_METHOD(GetDamage)
		{
			Asteroid asteroid(0, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Assert::IsTrue(asteroid.GetDamage() == 0);
		}
		TEST_METHOD(GetSize)
		{
			Asteroid asteroid(0, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Assert::IsTrue(Asteroid::Size::GIANT == asteroid.GetSize());
		}
		TEST_METHOD(GetPosition)
		{
			Asteroid asteroid(0, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Assert::IsTrue(asteroid.GetPosition() == Position(0, 0));
		}
		TEST_METHOD(GetSpeed)
		{
			Asteroid asteroid(0, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Assert::IsTrue(asteroid.GetSpeed() == 0);
		}
		TEST_METHOD(GetAngle)
		{
			Asteroid asteroid(0, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Assert::IsTrue(asteroid.GetAngle() == 0);
		}
		TEST_METHOD(isAlive)
		{
			Asteroid asteroid(5, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Assert::IsTrue(asteroid.isAlive() == true);
		}
		TEST_METHOD(OperatorEqual)
		{
			Asteroid asteroid1(0, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Asteroid asteroid2(asteroid1);
			Assert::IsTrue(asteroid1.GetSize() == asteroid2.GetSize());
		}
		TEST_METHOD(CopyConstructor)
		{
			Asteroid asteroid1(0, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Asteroid asteroid2 = asteroid1;
			Assert::IsTrue(asteroid1.GetSize() == asteroid2.GetSize());
		}
		TEST_METHOD(Move)
		{
			Asteroid asteroid1(0, 0, Asteroid::Size::GIANT, Position(0, 0), 1, 0, true);
			asteroid1.SetDirection(0, 1);
			asteroid1.SetSpeed(1);
			asteroid1.move();
			Assert::IsTrue(asteroid1.GetPosition() == Position(0, 1));
		}
	};
}