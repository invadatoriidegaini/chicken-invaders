#include "pch.h"
#include "CppUnitTest.h"
#include "../Chicken Invaders/chicken.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChickenInvadersTests
{
	TEST_CLASS(ChickenTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			chicken chicken(Position(0,0),5,true);
			Assert::IsTrue(chicken.getHealth() == 5);
			Assert::IsTrue(chicken.getPosition() == Position(0,0));
			Assert::IsTrue(chicken.isAlive() == true);
		}
		TEST_METHOD(GetHealth)
		{
			chicken chicken(Position(0, 0), 0, true);
			Assert::IsTrue(chicken.getHealth() == 0);
		}
		TEST_METHOD(GetPosition)
		{
			chicken chicken(Position(0, 0), 0, true);
			Assert::IsTrue(chicken.getPosition() == Position(0,0));
		}
		TEST_METHOD(isAlive)
		{
			chicken chicken(Position(0, 0), 5, true);
			Assert::IsTrue(chicken.isAlive()==true);
		}
		TEST_METHOD(Move)
		{
			chicken chicken(Position(0, 0), 0, true);
			chicken.move(1, 1);
			Assert::IsTrue(chicken.getPosition() == Position(1, 1));
		}

	};
}