#include "pch.h"
#include "CppUnitTest.h"
#include "../Chicken Invaders/Rockets.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChickenInvadersTests
{
	TEST_CLASS(RocketsTests)
	{
	public:

		TEST_METHOD(Constructor)
		{
			Rockets rockets(0, 0,Position(0,0));
			Assert::IsTrue(rockets.getDamage() == 0);
			Assert::IsTrue(rockets.getNumber() == 0);
			Assert::IsTrue(rockets.getPosition() == Position(0, 0));
		}
		TEST_METHOD(GetDamage)
		{
			Rockets rockets(0, 0, Position(0, 0));
			Assert::IsTrue(rockets.getDamage() == 0);
		}
		TEST_METHOD(GetNumber)
		{
			Rockets rockets(0, 0, Position(0, 0));
			Assert::IsTrue(rockets.getNumber() == 0);
		}
		TEST_METHOD(GetPosition)
		{
			Rockets rockets(0, 0, Position(0, 0));
			Assert::IsTrue(rockets.getPosition() == Position(0, 0));
		}
		TEST_METHOD(Move)
		{
			Rockets rockets(0, 0, Position(0, 0));
			rockets.move(1, 1);
			Assert::IsTrue(rockets.getPosition() == Position(1, 1));
		}
		TEST_METHOD(OperatorMinusMinus)
		{
			Rockets rockets(0, 1, Position(0, 0));
			rockets.operator--();
			Assert::IsTrue(rockets.getNumber() == 0);
		}
		TEST_METHOD(OperatorEqual)
		{
			Rockets rockets1(0, 0, Position(0, 0));
			Rockets rockets2(rockets1);
			Assert::IsTrue(rockets1.getDamage() == rockets2.getDamage());
			Assert::IsTrue(rockets1.getNumber() == rockets2.getNumber());
			Assert::IsTrue(rockets1.getPosition() == rockets2.getPosition());
		}

	};
}