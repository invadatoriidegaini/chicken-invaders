#include "pch.h"
#include "CppUnitTest.h"
#include "../Chicken Invaders/Gun.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChickenInvadersTests
{
	TEST_CLASS(GunTests)
	{
	public:
		TEST_METHOD(Constructor)
		{
			Position pos(0, 0);
			Gun gun(static_cast<uint8_t>(1), static_cast < uint8_t>(1), 1,pos);
			Assert::IsTrue(gun.getDamage() == 1);
			Assert::IsTrue(gun.getlevel() == 1);
			Assert::IsTrue(gun.getSpeed() == 1);
			Assert::IsTrue(gun.getPosition() == Position(0, 0));
		}
		TEST_METHOD(GetDamage)
		{
			Gun gun(1, 1, 1, Position(0, 0));
			Assert::IsTrue(gun.getDamage() == 1);
		}
		TEST_METHOD(GetLevel)
		{
			Gun gun(1, 1, 1, Position(0, 0));
			Assert::IsTrue(gun.getDamage() == 1);
		}
		TEST_METHOD(GetSpeed)
		{
			Gun gun(1, 1, 1.f, Position(0, 0));
			Assert::IsTrue(gun.getSpeed() == 1.f);
		
		}
		TEST_METHOD(GetPosition)
		{
			Gun gun(1, 1, 1, Position(0, 0));
			Assert::IsTrue(gun.getPosition() == Position(0, 0));
		}
		TEST_METHOD(move)
		{
			Gun gun(1, 1, 1, Position(0, 0));
			gun.move(1, 1);
			Assert::IsTrue(gun.getPosition() == Position(1, 1));
		}
	};
}