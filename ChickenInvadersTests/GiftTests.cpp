#include "pch.h"
#include "CppUnitTest.h"
#include "../Chicken Invaders/Gift.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChickenInvadersTests
{
	TEST_CLASS(GiftTests)
	{
	public:
		TEST_METHOD(Constructor)
		{
			Gift gift(Position(0, 0));
			Assert::IsTrue(gift.getPosition() == Position(0, 0));
		}
		TEST_METHOD(Move)
		{	
			Gift gift(Position(0, 0));
			gift.move(1, 1);
			Assert::IsTrue(gift.getPosition() == Position(1, 1));
		}
	};
}