#include "pch.h"
#include "CppUnitTest.h"
#include "../Chicken Invaders/Collision.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChickenInvadersTests
{
	TEST_CLASS(CollisionTests)
	{
	public:
		TEST_METHOD(playerEggColliding)
		{ 
			
			Player player;
			player.setPosition(Position(0, 0));
			Egg egg(Position(0,0),1);
			Assert::IsTrue(Collision::playerEggColliding(player,egg));
		}
		TEST_METHOD(playerChickenColliding)
		{
			Player player;
			player.setPosition(Position(0, 0));
			chicken chicken(Position(0, 0), 0, true);
			Assert::IsTrue(Collision::playerChickenColliding(player, chicken));
		}
		TEST_METHOD(playerAsteroidColliding)
		{
			Player player;
			player.setPosition(Position(0, 0));
			Asteroid asteroid(0, 0, Asteroid::Size::GIANT, Position(0, 0), 0, 0, true);
			Assert::IsTrue(Collision::playerAsteroidColliding(player, asteroid));
		}
		TEST_METHOD(giftCollidingPlayer)
		{
			Gift gift(Position(0, 0));
			Player player;
			player.setPosition(Position(0, 0));
			Assert::IsTrue(Collision::giftCollidingPlayer(gift, player));
		}
		TEST_METHOD(playerCollidingFood)
		{

			Player player;
			player.setPosition(Position(0, 0));
			Food food(5, 3, Position(0, 0));
			Assert::IsTrue(Collision::playerCollidingFood(food, player));
		}
	};
}